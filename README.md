# Description #

Functional Utility Library, applying Fantasy Land standards.

## Components ##

* Applicative functors
* Category concept
* Functors, Endofunctors
* Monads
* Monoid/Semigroup (in progress)

## Ideas ##

Separation of *Identity*, *Value* and *State*. Why? Default Javascript behaviour typically combines three of these together, for example: ``` var name = "Maciej"; ``` declaration gives identity, assigns the value, and modifies the state. The aim of the Fantasy Land Specification is to separate these three concepts, so that there is no state interference when it's not needed (I/O accessors are available in a controlled, and sparse manner). Each sequential data modification creates a new private environment, and a new identity for execution. Persistence is thus achieved by memory referencing old values.